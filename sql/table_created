CREATE TABLE public.app_project
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    description character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    name character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    user_id character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    created_date timestamp without time zone,
    CONSTRAINT app_project_pkey PRIMARY KEY (id),
    CONSTRAINT user_id FOREIGN KEY (user_id)
        REFERENCES public.app_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.app_project
    OWNER to postgres;

CREATE TABLE public.app_session
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    signature character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    "timestamp" bigint,
    user_id character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT app_session_pkey PRIMARY KEY (id),
    CONSTRAINT user_id FOREIGN KEY (user_id)
        REFERENCES public.app_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.app_session
    OWNER to postgres;

CREATE TABLE public.app_task
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    start_date timestamp without time zone,
    finish_date timestamp without time zone,
    description character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    name character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    project_id character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    user_id character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    created_date timestamp without time zone,
    CONSTRAINT app_task_pkey PRIMARY KEY (id),
    CONSTRAINT project_id FOREIGN KEY (project_id)
        REFERENCES public.app_project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT user_id FOREIGN KEY (user_id)
        REFERENCES public.app_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.app_task
    OWNER to postgres;

CREATE TABLE public.app_user
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default",
    first_name character varying(255) COLLATE pg_catalog."default",
    login character varying(255) COLLATE pg_catalog."default",
    middle_name character varying(255) COLLATE pg_catalog."default",
    password_hash character varying(255) COLLATE pg_catalog."default",
    role character varying(255) COLLATE pg_catalog."default",
    last_name character varying(255) COLLATE pg_catalog."default",
    locked boolean,
    CONSTRAINT app_user_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.app_user
    OWNER to postgres;