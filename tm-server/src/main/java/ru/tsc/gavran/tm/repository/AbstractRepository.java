package ru.tsc.gavran.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    protected AbstractRepository (@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected abstract E fetch (@Nullable final ResultSet row);

    public abstract E add(@NotNull final E entity);

    @Override
    @SneakyThrows
    public void remove(@NotNull final E entity) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "DELETE FROM " + getTableName();
        statement.executeUpdate(query);
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "delete from " + getTableName() + " where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final List<E> entities) {
        if (entities == null) return;
        for (@NotNull final E e : entities)
            add(e);
    }

    public Timestamp prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        java.sql.Timestamp mysqlDate = new java.sql.Timestamp(date.getTime());
        return mysqlDate;
    }

}
