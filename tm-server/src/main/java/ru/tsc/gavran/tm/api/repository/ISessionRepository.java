package ru.tsc.gavran.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains (@NotNull String id);

    @Nullable
    Session update(@Nullable Session entity);

}
