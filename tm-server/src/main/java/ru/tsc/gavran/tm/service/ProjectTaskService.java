package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IProjectRepository;
import ru.tsc.gavran.tm.api.repository.ITaskRepository;
import ru.tsc.gavran.tm.api.service.IConnectionService;
import ru.tsc.gavran.tm.api.service.IProjectTaskService;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.empty.EmptyIndexException;
import ru.tsc.gavran.tm.exception.empty.EmptyNameException;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.repository.ProjectRepository;
import ru.tsc.gavran.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task bindTaskById(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task unbindTaskById(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, final @NotNull int index) {
        if (index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
