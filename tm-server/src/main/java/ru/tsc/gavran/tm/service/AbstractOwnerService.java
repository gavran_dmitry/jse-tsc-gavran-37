package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.api.service.IConnectionService;
import ru.tsc.gavran.tm.api.service.IOwnerService;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    public AbstractOwnerService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public abstract IOwnerRepository<@Nullable E> getRepository(@NotNull Connection connection);

    @Override
    @SneakyThrows
    public @NotNull E add(@NotNull final String userId, final @Nullable E entity) {
        if (entity == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            final @NotNull @Nullable E entityResult = repository.add(userId, entity);
            connection.commit();
            return entityResult;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, final @Nullable E entity) {
        if (entity == null) throw new ProcessException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            repository.remove(userId, entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            return repository.findAll(userId);
        }
        catch (@NotNull final Exception e) {
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            return repository.findById(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public  void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            final IOwnerRepository<@Nullable E> repository = getRepository(connection);
            repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return findById(userId, id) != null;
    }

}