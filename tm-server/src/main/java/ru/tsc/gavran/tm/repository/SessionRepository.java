package ru.tsc.gavran.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.ISessionRepository;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository (@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "app_session";
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull String id) {
       @NotNull final String query = "SELECT * FROM " +
               "WHERE id = ? LIMIT 1";
       @NotNull final PreparedStatement statement = connection.prepareStatement(query);
       statement.setString(1, id);
       final boolean result = statement.executeQuery().isFirst();
       statement.close();
       return result;
    }

    @Override
    @SneakyThrows
    public Session fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(row.getString("user_id"));
        session.setSignature(row.getString("signature"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setId(row.getString("id"));
        return session;
    }

    @Override
    @SneakyThrows
    public Session add(@NotNull Session session) {
        if (session == null) return null;
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, user_id, signature, timestamp) " +
                " values(?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setLong(4, session.getTimestamp());
        statement.executeUpdate();
        statement.close();
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session update(@Nullable Session entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                " id=?, timestamp=?, signature=?, user_id=? where id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getUserId());
        statement.setString(5, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull Session entity) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? ";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }


}
