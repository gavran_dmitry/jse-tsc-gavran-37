package ru.tsc.gavran.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(final E entity);

    void remove(final E entity);

    @NotNull
    List<E> findAll();

    void clear();

    @Nullable
    E findById(@NotNull final String id);

    @NotNull
    void removeById(@NotNull final String id);

    void addAll(@Nullable List<E> entities);



}