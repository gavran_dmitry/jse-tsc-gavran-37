package ru.tsc.gavran.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId);

    @NotNull
    Task unbindTaskById(@NotNull final String userId, @NotNull final String id);

    void unbindAllTaskByProjectId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Task findByName(@NotNull final String userId, @NotNull String name);

    void removeByName(@NotNull final String userId, @NotNull String name);

    @Nullable
    Task startById(@NotNull final String userId, @NotNull String id);

    @Nullable
    Task startByName(@NotNull final String userId, @NotNull String name);

    @Nullable
    Task startByIndex(@NotNull final String userId, @NotNull int index);

    @Nullable
    Task finishById(@NotNull final String userId, @NotNull String id);

    @Nullable
    Task finishByName(@NotNull final String userId, @NotNull String name);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull final String id);

    @Nullable
    Task finishByIndex(@NotNull final String userId, @NotNull int index);

    @Nullable
    Task changeStatusById(@NotNull final String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Task changeStatusByName(@NotNull final String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task changeStatusByIndex(@NotNull final String userId, @NotNull int index, @NotNull Status status);

    @Nullable
    Task update(@Nullable Task entity);

    @Nullable
    void removeByIndex(@NotNull final String userId, @NotNull int index);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull int index);

}