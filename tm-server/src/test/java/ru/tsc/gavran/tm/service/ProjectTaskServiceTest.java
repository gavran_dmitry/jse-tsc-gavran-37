//package ru.tsc.gavran.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.gavran.tm.model.Project;
//import ru.tsc.gavran.tm.model.Task;
//import ru.tsc.gavran.tm.repository.ProjectRepository;
//import ru.tsc.gavran.tm.repository.TaskRepository;
//
//import java.util.List;
//
//public class ProjectTaskServiceTest {
//
//    @Nullable
//    private ProjectTaskService projectTaskService;
//
//    @Nullable
//    private ProjectRepository projectRepository;
//
//    @Nullable
//    private TaskRepository taskRepository;
//
//    @Nullable
//    private Project project;
//
//    @Nullable
//    private Task task;
//
//    @NotNull
//    protected static final String TEST_TASK_NAME = "TestProjectTaskName";
//
//    @NotNull
//    protected static final String TEST_PROJECT_NAME = "TestProjectTaskName";
//
//    @NotNull
//    protected static final String TEST_PROJECT_TASK_DESCRIPTION = "TestProjectTaskDescription";
//
//    @NotNull
//    protected static final String TEST_USER_ID = "TestProjectTaskUserId";
//
//    @Before
//    public void before() {
//        projectRepository = new ProjectRepository();
//        taskRepository = new TaskRepository();
//        project = projectRepository.add(TEST_USER_ID, new Project(TEST_PROJECT_NAME, TEST_PROJECT_TASK_DESCRIPTION));
//        task = taskRepository.add(TEST_USER_ID, new Task(TEST_TASK_NAME, TEST_PROJECT_TASK_DESCRIPTION));
//        task.setProjectId(project.getId());
//        projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getId());
//        Assert.assertNotNull(task.getName());
//        Assert.assertNotNull(task.getDescription());
//        Assert.assertNotNull(task.getProjectId());
//        Assert.assertEquals(TEST_TASK_NAME, task.getName());
//        Assert.assertEquals(TEST_PROJECT_TASK_DESCRIPTION, task.getDescription());
//
//        @NotNull final Task taskById = taskRepository.findById(task.getId());
//        Assert.assertNotNull(taskById);
//        Assert.assertEquals(task, taskById);
//
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getId());
//        Assert.assertNotNull(project.getName());
//        Assert.assertNotNull(project.getDescription());
//        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
//        Assert.assertEquals(TEST_PROJECT_TASK_DESCRIPTION, project.getDescription());
//
//        @NotNull final Project projectById = projectRepository.findById(project.getId());
//        Assert.assertNotNull(projectById);
//        Assert.assertEquals(project, projectById);
//    }
//    @Test
//    public void findTaskByProjectId() {
//        @NotNull final List<Task> tasks = projectTaskService.findTaskByProjectId(TEST_USER_ID, project.getId());
//        Assert.assertNotNull(task);
//        Assert.assertEquals(task.getProjectId(), project.getId());
//    }
//
//    @Test
//    public void bindTaskById() {
//        task.setProjectId(null);
//        @NotNull final Task bindTask = projectTaskService.bindTaskById(TEST_USER_ID, project.getId(), task.getId());
//        Assert.assertNotNull(bindTask);
//        Assert.assertNotNull(bindTask.getProjectId());
//        Assert.assertEquals(bindTask.getProjectId(), project.getId());
//    }
//
//    @Test
//    public void unbindTaskById() {
//        task.setProjectId(project.getId());
//        @NotNull final Task bindTask = projectTaskService.unbindTaskById(TEST_USER_ID, project.getId(), task.getId());
//        Assert.assertNotNull(bindTask);
//        Assert.assertNull(bindTask.getProjectId());
//    }
//
//    @Test
//    public void removeProjectById() {
//        projectTaskService.removeProjectById(TEST_USER_ID, project.getId());
//        Assert.assertNull(task.getProjectId());
//    }
//
//    @Test
//    public void removeProjectByIndex() {
//        projectTaskService.removeProjectByIndex(TEST_USER_ID, 0);
//        Assert.assertNull(task.getProjectId());
//    }
//
//    @Test
//    public void removeProjectByName() {
//        projectTaskService.removeProjectByName(TEST_USER_ID, project.getName());
//        Assert.assertNull(task.getProjectId());
//    }
//
//}
