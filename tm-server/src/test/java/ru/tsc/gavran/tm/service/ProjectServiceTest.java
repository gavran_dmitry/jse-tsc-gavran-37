//package ru.tsc.gavran.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.gavran.tm.api.service.IConnectionService;
//import ru.tsc.gavran.tm.component.Bootstrap;
//import ru.tsc.gavran.tm.enumerated.Status;
//import ru.tsc.gavran.tm.model.Project;
//import ru.tsc.gavran.tm.model.Session;
//import ru.tsc.gavran.tm.repository.ProjectRepository;
//
//import java.util.List;
//
//public class ProjectServiceTest {
//
//    @Nullable
//    private static ProjectService projectService;
//
//    @Nullable
//    private static Project project;
//
//    @Nullable
//    private SessionService sessionService;
//
//    @NotNull
//    private Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private Session session;
//
//    @NotNull
//    protected static final String TEST_PROJECT_NAME = "ProjectTestName";
//
//    @NotNull
//    protected static final String TEST_DESCRIPTION = "ProjectTestDescription";
//
//    @NotNull
//    protected static final String TEST_USER_ID = "ProjectTestUserId";
//
//    @Before
//    public void before() {
//        projectService = new ProjectService(new ConnectionService(new PropertyService()));
//        ConnectionService connectionService = new ConnectionService(new PropertyService());
//        this.session = sessionService.open("postgres", "09041991");
//        project = projectService.add(session.getUserId(), new Project(TEST_PROJECT_NAME, TEST_DESCRIPTION));
//    }
//
//    @Test
//    public void findById() {
//        @NotNull final Project projects = projectService.findById(project.getId());
//        Assert.assertEquals(TEST_PROJECT_NAME, projects.getName());
//
//    }
//    @After
//    public void after() {
//        projectService.remove(session.getUserId(), project);
//        sessionService.close(session);
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Project> projects = projectService.findAll();
//        Assert.assertEquals(1, projects.size());
//    }
//
//    @Test
//    public void create() {
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getId());
//        Assert.assertNotNull(project.getName());
//        Assert.assertNotNull(project.getDescription());
//
//        @NotNull final Project taskById = projectService.findById(project.getId());
//        Assert.assertNotNull(taskById);
//        Assert.assertEquals(project, taskById);
//    }
//
//
//    @Test
//    public void changeStatusById() {
//        @Nullable final Project project = projectService.changeStatusById(TEST_USER_ID, this.project.getId(), Status.NOT_STARTED);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
//    }
//
//    @Test
//    public void changeStatusByName() {
//        @Nullable final Project project = projectService.changeStatusByName(TEST_USER_ID, TEST_PROJECT_NAME, Status.NOT_STARTED);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
//    };
//
//    @Test
//    public void changeStatusByIndex() {
//        @Nullable final Project project = projectService.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
//    };
//
//    @Test
//    public void findByName() {
//        @Nullable final Project project = projectService.findByName(TEST_USER_ID, TEST_PROJECT_NAME);
//        Assert.assertNotNull(project);
//    };
//
//    @Test
//    public void removeByIndex() {
//        @Nullable final Project project = projectService.removeByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(project);
//    };
//
//    @Test
//    public void updateByIndex() {
//        @Nullable final Project project = projectService.updateByIndex(TEST_USER_ID,0, TEST_PROJECT_NAME, TEST_DESCRIPTION);
//        Assert.assertNotNull(project);
//        Assert.assertEquals(project.getDescription(), TEST_DESCRIPTION);
//    };
//
//    @Test
//    public void updateById() {
//        @Nullable final Project project = projectService.updateById(TEST_USER_ID,this.project.getId(), TEST_PROJECT_NAME, TEST_DESCRIPTION);
//        Assert.assertNotNull(project);
//        Assert.assertEquals(project.getDescription(), TEST_DESCRIPTION);
//    };
//
//    @Test
//    public void startById() {
//        @Nullable final Project project = projectService.startById(TEST_USER_ID, this.project.getId());
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
//    };
//
//    @Test
//    public void startByName() {
//        @Nullable final Project project = projectService.startByName(TEST_USER_ID, TEST_PROJECT_NAME);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
//    };
//
//    @Test
//    public void startByIndex() {
//        @Nullable final Project project = projectService.startByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
//    };
//
//    @Test
//    public void finishById() {
//        @Nullable final Project project = projectService.finishById(TEST_USER_ID, this.project.getId());
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.COMPLETED, project.getStatus());
//    };
//
//    @Test
//    public void finishByName() {
//        @Nullable final Project project = projectService.finishByName(TEST_USER_ID, TEST_PROJECT_NAME);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.COMPLETED, project.getStatus());
//    };
//
//    @Test
//    public void finishByIndex() {
//        @Nullable final Project project = projectService.finishByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(project);
//        Assert.assertNotNull(project.getStatus());
//        Assert.assertEquals(Status.COMPLETED, project.getStatus());
//    };
//}
