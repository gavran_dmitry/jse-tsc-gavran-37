package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.service.ConnectionService;
import ru.tsc.gavran.tm.service.PropertyService;
import ru.tsc.gavran.tm.service.SessionService;

import java.util.List;

@Ignore
public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Project project;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
        projectRepository = new ProjectRepository(connectionService.getConnection());
        project = new Project();
        project.setName(TEST_PROJECT_NAME);
        project.setDescription(TEST_DESCRIPTION_NAME);
        project = projectRepository.add(session.getUserId(), project);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @NotNull final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(projectRepository.existsById(TEST_USER_ID, project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll(TEST_USER_ID);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = projectRepository.findById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
    }


    @Test
    public void findByName() {
        @NotNull final Project project = projectRepository.findByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
    }

    @Test
    public void startById() {
        @Nullable final Project project = projectRepository.startById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByName() {
        @Nullable final Project project = projectRepository.startByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByIndex() {
        @Nullable final Project project = projectRepository.startByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void finishById() {
        @Nullable final Project project = projectRepository.finishById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void finishByName() {
        @Nullable final Project project = projectRepository.finishByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void finishByIndex() {
        @Nullable final Project project = projectRepository.finishByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void changeStatusById() {
        @Nullable final Project project = projectRepository.changeStatusById(TEST_USER_ID, this.project.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Project project = projectRepository.changeStatusByName(TEST_USER_ID, TEST_PROJECT_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Project project = projectRepository.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void removeById() {
        projectRepository.removeById(TEST_USER_ID, project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

}