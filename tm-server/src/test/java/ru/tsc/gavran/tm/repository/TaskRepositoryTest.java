//package ru.tsc.gavran.tm.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.gavran.tm.component.Bootstrap;
//import ru.tsc.gavran.tm.enumerated.Status;
//import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
//import ru.tsc.gavran.tm.exception.system.ProcessException;
//import ru.tsc.gavran.tm.model.Session;
//import ru.tsc.gavran.tm.model.Task;
//import ru.tsc.gavran.tm.service.SessionService;
//
//import java.util.List;
//
//public class TaskRepositoryTest {
//
//    @Nullable
//    private TaskRepository taskRepository;
//
//    @Nullable
//    private SessionService sessionService;
//
//    @NotNull
//    private Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private Task task;
//
//    @Nullable
//    private Session session;
//
//    @NotNull
//    protected static final String TEST_TASK_NAME = "TestName";
//
//    @NotNull
//    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";
//
//    @NotNull
//    protected static final String TEST_USER_ID = "TestUserId";
//
//    @Before
//    public void before() {
//        taskRepository = new TaskRepository();
//        task = taskRepository.add(TEST_USER_ID, new Task(TEST_TASK_NAME, TEST_DESCRIPTION_NAME));
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getId());
//        Assert.assertNotNull(task.getName());
//        Assert.assertNotNull(task.getDescription());
//        Assert.assertEquals(TEST_TASK_NAME, task.getName());
//        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());
//
//        @NotNull final Task taskById = taskRepository.findById(task.getId());
//        Assert.assertNotNull(taskById);
//        Assert.assertEquals(task, taskById);
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Task> tasks = taskRepository.findAll();
//        Assert.assertEquals(1, tasks.size());
//    }
//
//    @Test
//    public void existsById() {
//        Assert.assertTrue(taskRepository.existsById(TEST_USER_ID, task.getId()));
//    }
//
//    @Test
//    public void findAllByUserId() {
//        @NotNull final List<Task> tasks = taskRepository.findAll(TEST_USER_ID);
//        Assert.assertEquals(1, tasks.size());
//    }
//
//    @Test
//    public void findById() {
//        @NotNull final Task task = taskRepository.findById(TEST_USER_ID, this.task.getId());
//        Assert.assertNotNull(task);
//    }
//
//    @Test
//    public void remove() {
//        taskRepository.removeById(task.getId());
//        Assert.assertNull(taskRepository.findById(task.getId()));
//    }
//
//    @Test
//    public void findByName() {
//        @NotNull final Task task = taskRepository.findByName(TEST_USER_ID, TEST_TASK_NAME);
//        Assert.assertNotNull(task);
//    }
//
//    @Test
//    public void findByIndex() {
//        @NotNull final Task task = taskRepository.findByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(task);
//    }
//
//    @Test
//    public void removeById() {
//        taskRepository.removeById(TEST_USER_ID, task.getId());
//        Assert.assertNull(taskRepository.findById(task.getId()));
//    }
//
//    @Test
//    public void removeByIndex() {
//        taskRepository.removeByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(task);
//    }
//
//    @Test
//    public void removeByName() {
//        taskRepository.removeByName(session.getUserId(), TEST_TASK_NAME);
//        Assert.assertNull(taskRepository.findByName(session.getUserId(), TEST_TASK_NAME));
//    }
//
//    @Test
//    public void startById() {
//        @Nullable final Task task = taskRepository.startById(TEST_USER_ID, this.task.getId());
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
//    }
//
//    @Test
//    public void startByName() {
//        @Nullable final Task task = taskRepository.startByName(TEST_USER_ID, TEST_TASK_NAME);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
//    }
//
//    @Test
//    public void startByIndex() {
//        @Nullable final Task task = taskRepository.startByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
//    }
//
//    @Test
//    public void finishById() {
//        @Nullable final Task task = taskRepository.finishById(TEST_USER_ID, this.task.getId());
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.COMPLETED, task.getStatus());
//    }
//
//    @Test
//    public void finishByName() {
//        @Nullable final Task task = taskRepository.finishByName(TEST_USER_ID, TEST_TASK_NAME);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.COMPLETED, task.getStatus());
//    }
//
//    @Test
//    public void finishByIndex() {
//        @Nullable final Task task = taskRepository.finishByIndex(TEST_USER_ID, 0);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.COMPLETED, task.getStatus());
//    }
//
//    @Test
//    public void changeStatusById() {
//        @Nullable final Task task = taskRepository.changeStatusById(TEST_USER_ID, this.task.getId(), Status.NOT_STARTED);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//    }
//
//    @Test
//    public void changeStatusByName() {
//        @Nullable final Task task = taskRepository.changeStatusByName(TEST_USER_ID, TEST_TASK_NAME, Status.NOT_STARTED);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//    }
//
//    @Test
//    public void changeStatusByIndex() {
//        @Nullable final Task task = taskRepository.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task.getStatus());
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//    }
//
//}