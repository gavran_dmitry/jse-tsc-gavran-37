//package ru.tsc.gavran.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.gavran.tm.component.Bootstrap;
//import ru.tsc.gavran.tm.model.Session;
//import ru.tsc.gavran.tm.model.User;
//import ru.tsc.gavran.tm.repository.SessionRepository;
//
//import java.util.List;
//
//public class SessionServiceTest {
//
//    @Nullable
//    private SessionService sessionService;
//
//    @NotNull
//    private Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private Session session;
//
//    @Nullable
//    private ConnectionService connectionService;
//
//    @Nullable
//    private User user = new User();;
//
//    @NotNull
//    protected static final String TEST_USER_LOGIN = "TestLogin";
//
//    @NotNull
//    protected static final String TEST_USER_PASSWORD = "TestUserPassword";
//
//    @Before
//    public void before() {
//        user = bootstrap.getUserService().create(TEST_USER_LOGIN, TEST_USER_PASSWORD);
//        sessionService = new SessionService(new SessionRepository(connection), bootstrap);
//        @NotNull final Session session = new Session();
//        session.setUserId(user.getId());
//        this.session = sessionService.add(session);
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNotNull(session);
//        Assert.assertNotNull(session.getId());
//        Assert.assertNotNull(session.getUserId());
//        Assert.assertEquals(user.getId(), session.getUserId());
//
//        @NotNull final Session sessionById = sessionService.findById(session.getId());
//        Assert.assertNotNull(sessionById);
//        Assert.assertEquals(session, sessionById);
//    }
//
//    @Test
//    public void findById() {
//        @NotNull final Session session = sessionService.findById(this.session.getId());
//        Assert.assertNotNull(session);
//    }
//
//    @Test
//    public void findAllByUserId() {
//        @NotNull final List<Session> session = sessionService.findAll(this.session.getUserId());
//        Assert.assertNotNull(session);
//        Assert.assertEquals(1, session.size());
//    }
//
//    @Test
//    public void remove() {
//        sessionService.removeById(session.getId());
//        Assert.assertNull(sessionService.findById(session.getId()));
//    }
//
//    @Test
//    public void removeById() {
//        sessionService.removeById(session.getId());
//        Assert.assertNull(sessionService.findById(session.getId()));
//    }
//
//    @Test
//    public void open() {
//        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
//        Assert.assertNotNull(session);
//    }
//
//    @Test
//    public void validate() {
//        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
//        sessionService.validate(session);
//        Assert.assertNotNull(session);
//    }
//
//    @Test
//    public void close() {
//        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
//        Assert.assertNotNull(session);
//        sessionService.close(session);
//        Assert.assertNull(sessionService.findById(session.getId()));
//    }
//
//}