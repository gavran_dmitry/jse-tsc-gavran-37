//package ru.tsc.gavran.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.gavran.tm.component.Bootstrap;
//import ru.tsc.gavran.tm.model.Session;
//import ru.tsc.gavran.tm.model.User;
//import ru.tsc.gavran.tm.repository.UserRepository;
//
//import java.util.List;
//
//public class UserServiceTest {
//
//    @Nullable
//    private UserService userService;
//
//    @Nullable
//    private User user;
//
//    @Nullable
//    private SessionService sessionService;
//
//    @NotNull
//    private Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private Session session;
//
//    @NotNull
//    protected static final String TEST_USER_LOGIN = "TestUserLogin";
//
//    @NotNull
//    protected static final String TEST_USER_EMAIL = "TestUserEmail";
//
//    @Before
//    public void before() {
//        userService = new UserService(new UserRepository(), new PropertyService());
//        @NotNull final User user = new User();
//        user.setLogin(TEST_USER_LOGIN);
//        user.setEmail(TEST_USER_EMAIL);
//        this.user = userService.add(user);
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(user.getId());
//        Assert.assertNotNull(user.getLogin());
//        Assert.assertEquals(TEST_USER_LOGIN, user.getLogin());
//        Assert.assertNotNull(user.getEmail());
//        Assert.assertEquals(TEST_USER_EMAIL, user.getEmail());
//
//        @Nullable final User userById = userService.findById(user.getId());
//        Assert.assertNotNull(userById);
//        Assert.assertEquals(user, userById);
//    }
//
//    @Test
//    public void findAll() {
//        @Nullable final List<User> users = userService.findAll();
//        Assert.assertEquals(1, users.size());
//    }
//
//    @Test
//    public void findById() {
//        @Nullable final User user = userService.findById(this.user.getId());
//        Assert.assertNotNull(user);
//    }
//
//    @Test
//    public void findByLogin() {
//        @Nullable final User user = userService.findByLogin(this.user.getLogin());
//        Assert.assertNotNull(user);
//    }
//
//    @Test
//    public void remove() {
//        userService.removeById(user.getId());
//        Assert.assertNull(userService.findById(user.getId()));
//    }
//
//    @Test
//    public void removeById() {
//        userService.removeById(user.getId());
//        Assert.assertNull(userService.findById(user.getId()));
//    }
//
//    @Test
//    public void removeUserByLogin() {
//        @Nullable final User user = userService.removeByLogin(this.user.getLogin());
//        Assert.assertNotNull(user);
//    }
//
//    @Test
//    public void isLoginExist() {
//        Assert.assertTrue(userService.isLoginExist(this.user.getLogin()));
//    }
//
//    @Test
//    public void isLoginExistFalse() {
//        Assert.assertFalse(userService.isLoginExist("test"));
//    }
//
//    @Test
//    public void isEmailExist() {
//        Assert.assertTrue(userService.isEmailExist(user.getEmail()));
//    }
//
//    @Test
//    public void isEmailExistFalse() {
//        Assert.assertFalse(userService.isEmailExist("email"));
//    }
//
//    @Test
//    public void setPassword() {
//        @NotNull final User user = userService.setPassword(this.user.getId(), "password");
//        Assert.assertNotNull(user);
//    }
//
//    @Test
//    public void lockUserByLogin() {
//        @NotNull final User user = userService.lockUserByLogin(this.user.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertTrue(user.getLocked());
//    }
//
//    @Test
//    public void unlockUserByLogin() {
//        @NotNull final User user = userService.unlockUserByLogin(this.user.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertFalse(user.getLocked());
//    }
//
//}