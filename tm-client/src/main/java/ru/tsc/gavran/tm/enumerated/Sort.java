package ru.tsc.gavran.tm.enumerated;

import ru.tsc.gavran.tm.comparator.ComparatorByCreated;
import ru.tsc.gavran.tm.comparator.ComparatorByName;
import ru.tsc.gavran.tm.comparator.ComparatorByStartDate;
import ru.tsc.gavran.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by Name.", ComparatorByName.getInstance()),
    CREATED("Sort by Created.", ComparatorByCreated.getInstance()),
    START_DATE("Sort by Start Date.", ComparatorByStartDate.getInstance()),
    STATUS("Sort by Status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}