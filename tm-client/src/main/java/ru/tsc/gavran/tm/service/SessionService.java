package ru.tsc.gavran.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.service.ISessionService;
import ru.tsc.gavran.tm.endpoint.Session;

@Getter
@Setter
public class SessionService implements ISessionService {

    @Nullable
    private Session session;

}
