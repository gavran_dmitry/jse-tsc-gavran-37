package ru.tsc.gavran.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.exception.AbstractException;
import ru.tsc.gavran.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws AbstractException {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

    String ANSI_RESET = "\u001B[0m";
    String ANSI_RED = "\u001B[31m";
    String ANSI_GREEN = "\u001B[32m";
    String AUTHORIZED = "You are not authorized. Enter one of the following commands: ";


}