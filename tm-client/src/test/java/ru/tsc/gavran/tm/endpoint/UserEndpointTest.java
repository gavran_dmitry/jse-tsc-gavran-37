package ru.tsc.gavran.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.marker.SoapCategory;

@Ignore
public class UserEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Test
    @Category(SoapCategory.class)
    public void existsUserByEmail() {
        final boolean user = bootstrap.getUserEndpoint().existsUserByEmail("EmailNewTest1");
        Assert.assertTrue(user);
    }

    @Test
    @Category(SoapCategory.class)
    public void existsUserByLogin() {
        @NotNull final boolean user = bootstrap.getUserEndpoint().existsUserByLogin("User");
        Assert.assertTrue(user);
}
}
